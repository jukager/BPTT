function [plot_handle] = multiple_smoothed_rates(BPTT_objects, legend_names, variable_name, order, framelen, varargin)
    if any(~ismember(varargin,'LineStyle'))
        varargin = [varargin, 'LineStyle', '-'];
    end
    
    if mod(framelen,2) == 0
        framelen = framelen + 1;
    end
    
    for index = 1:numel(BPTT_objects)
        BPTT_objects{index}.Variables.(variable_name).Data = sgolayfilt(BPTT_objects{index}.Variables.(variable_name).Data, order, framelen);
    end
    plot_handle = BPTT.plots.time_variables(BPTT_objects, legend_names, variable_name, varargin{:});
    hold on
    BPTT.plots.tools.horizontal_line(0,'--');
    hold off
    
    dim = [.2 .5 .3 .3];
    str = ['Smoothed with Salvitzky-Golay filter \newline order = ' num2str(order) '; frame length = ' num2str(framelen)];
    annotation('textbox',dim,'String',str,'FitBoxToText','on');
    
end