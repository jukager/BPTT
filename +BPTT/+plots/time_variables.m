function [plot_handle] = time_variables(BPTT_objects, legend_names, variable_name, varargin)
    % Plot variable with name "variable_name" for all BPTT objects in the
    % "BPTT_objects" array. The legend is used with the names in the
    % character array "legend_names". If the legend is not wanted just use
    % 'none' instead.
    % If one wants to normalize the data (set maximum to 1 and minimum to 
    % 0), simply add "normalized" to varargin.
    
    hold on
    for index = 1:numel(BPTT_objects)
        if any(ismember(varargin,'Unit'))
            obj.p = struct;
            obj.p.output_unit = varargin{find(ismember(varargin,'Unit'))+1};
            x.time_var = BPTT_objects{index}.Variables.(variable_name);
            y = BPTT.calculators.calc_convert_variables(x,obj);
            plot_object = y.time_var_conv;
        else
            plot_object = BPTT_objects{index}.Variables.(variable_name);
        end
        
        if any(strcmp(varargin,'normalized'))
            plot_object.sdev = (plot_object.sdev)/(max(plot_object.Data)-min(plot_object.Data));
            plot_object.Data = (plot_object.Data-min(plot_object.Data))/(max(plot_object.Data)-min(plot_object.Data));;
        end
        
        if any(ismember(varargin,'LineStyle'))
            plot_style = varargin{find(ismember(varargin,'LineStyle'))+1};
        else
            plot_style = '-';
        end
        
        if ~isempty(plot_object.sdev)
            if size(plot_object.Data,2) <= 1
                errorbar(plot_object.Time,plot_object.Data, plot_object.sdev,plot_style);
            else
                for replicate_idx = 1:size(plot_object.Data,2)
                    errorbar(plot_object.Time, plot_object.Data(:,replicate_idx),plot_object.sdev(:,replicate_idx), plot_style);
                end
            end
        else
            plot_object.plot(plot_style);
        end
        
        if any(strcmp(varargin,'normalized'))
            ylim([0,1]);
        end
        
    end
    hold off
    box on;
    create_legend(legend_names)
    
    x_unit = BPTT_objects{index}.Variables.(variable_name).TimeInfo.Unit;
    xlabel(['Process time (' x_unit ')']);
    
    y_unit = plot_object.DataInfo.Units;
    y_name = BPTT_objects{index}.Variables.(variable_name).Name;
    ylabel([ y_name ' (' y_unit ')']);
    
    plot_handle = gcf;
    
end

function [legend_handle] = create_legend(legend_names)
    if isa(legend_names,'cell')
        legend(legend_names)
    end
end