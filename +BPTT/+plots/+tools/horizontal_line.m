function [handle] = horizontal_line(y_value, varargin)
    if isempty(varargin)
        varargin{1} = '-';
    end
    ax = gca;
    xlim = ax.XLim;
    handle = line(xlim, [y_value, y_value],'LineStyle',varargin{1});
    set(handle,'Color','k');
end