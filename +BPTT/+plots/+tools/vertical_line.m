function [handle] = vertical_line(x_value, varargin)
    if ~isempty(varargin)
        line_style = varargin{1};
    else
        line_style = '--';
    end
    ax = gca;
    y_lim = ax.YLim;
    handle = line([x_value, x_value], y_lim,'LineStyle',line_style);
end