function  output  = K2S1_v2(Fs, c_s, M_s, F_a_in, F_o2_in, y_o2_out, y_co2_out, y_wet, y_o2_in_air, y_co2_in_air, gamma_s, gamma_o2, gamma_x, Vol, rx_ext, e_x, e_o2, e_co2, e_s)      
                    
    %----------------inputs:---------------------------------------------------------------------------------------------------------------------------------------------------------------------
%0.088,2.5,0.4,0.15,0.1,823.075,30.66, 0.2073,0.2094,0.00039,4.666,-4,4.23,1
% calculated variables (internally)---------------------------------------- 

y_o2_in=(F_a_in.*y_o2_in_air+F_o2_in)./(F_a_in+F_o2_in);
y_co2_in=(F_a_in.*y_co2_in_air)./(F_a_in+F_o2_in);

ex_h2o=1-(y_wet./y_o2_in_air);
Ra_inert = (1-y_o2_in-y_co2_in)./(1-y_o2_out-y_co2_out-ex_h2o);
V_m=22.4;

% calculated rates---------------------------------------------------------

r_s = -(Fs*c_s)/M_s;%rsm in C-mol/h
r_co2 = (((F_a_in+F_o2_in).*60)./V_m).*(y_co2_out.*Ra_inert-y_co2_in);
r_o2 = (((F_a_in+F_o2_in).*60)./V_m).*(y_o2_out.*Ra_inert-y_o2_in);


xm=[r_s; r_o2; r_co2];
E=[+1 0 +1 +1;gamma_s gamma_o2 gamma_x 0];
Em=[+1,0,+1;gamma_s,gamma_o2,0]; % First row C balance 2nd row DR balance
Ec =[1; gamma_x]; % Biomass Estimation

Xi=[e_s 0 0;0 e_o2 0 ;0 0 e_co2]; % Matrix with relative error terms e_s = substrate feed e_o2 = O2 measurement e_CO2 = CO2 measurement
   
Ec_star=(inv(Ec'*Ec))*Ec';

R=Em-Ec*Ec_star*Em;
[U,S,V]=svd(R);
Sconv=[1 0];
C=Sconv*S;
K=C*S'*U';
Rred=K*R;
eps = Rred * xm;
sai = diag(diag(xm * xm' * Xi * Xi'));
Phi = Rred * sai *Rred';
delta = (sai*Rred'*inv(Phi) * Rred)* xm;
xmbest=xm-delta;
xcbest = -Ec_star*Em*xmbest;
h = eps' * inv(Phi) * eps;

rs_ox = rx_ext + e_x + 1; % surplus Input output equation
%Outputs----------------------------------------------------------------
%h_value------------------------------------------------------------------
h_value=h;

% valumetric reconciled rates---------------------------------------------
rs_rec=xmbest(1,1)./Vol;         % rS
ro2_rec=xmbest(2,1)./Vol;        % OUR
rco2_rec=xmbest(3,1)./Vol;       % CER
rx_rec=xcbest./Vol;              % rX
% reconciled yields-------------------------------------------------------
Yx_s=-rx_rec./rs_rec;
Y_o2_s=ro2_rec./rs_rec;
Y_co2_s=-rco2_rec./rs_rec;
Y_o2_x=-ro2_rec./rx_rec;
Y_co2_x=rco2_rec./rx_rec;

output=[h_value,rs_rec,ro2_rec,rco2_rec,rx_rec,Yx_s,Y_o2_s,Y_co2_s,Y_o2_x,Y_co2_x,rs_ox];

end


  
  
  
 
 
 
 
  
  
  
  


