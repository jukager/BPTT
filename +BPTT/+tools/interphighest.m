% Vienna University of Technology, Bioprocess Engineering
% Aydin Golabgir last modified: 01.09.2014
% interplowest is a function for interpolating two or more TimeVariables using the lowest sampling frequency
% Input TS is a structure containing the TimeVariable objects
% Output TS_out is a structure containing the interpolated TimeVariable objects

function TS_int = interphighest(TS)

    input_names = fieldnames(TS);
    
    
    % extract the time series objects from the Variables structure
    for i=1:length(input_names)
        
         %delete all non-monotonically increasing points in inputs
        idx = diff(TS.(input_names{i}).Time)==0;
        
        idx = find(idx);
        TS.(input_names{i}) = delsample(TS.(input_names{i}),'Index',idx);

        % selecting the common_time based on the TimeVariable with lowest
        % data points. If interplowest property of the TimeVariable is set
        % to zero, it will be excluded from this procedure
        
        if TS.(input_names{i}).interplowest == 1
            
            len(i) = TS.(input_names{i}).Length;
        else
            len(i) = 1e24; % an arbitary large number to exclude the variable from the upcoming min selection
        end
        
    end
    
    % find the index of the timeseries with the least number of
    % data points and set the common time axis to that

    idx = find(len==max(len));
    common_time = TS.(input_names{idx(1)}).Time;

   
    for i=1:length(input_names)
        % interpolation using the resample function
        %TS_int.(input_names{i}) = resample(TS.(input_names{i}),common_time);
        
        % create new timeseries objects from the input
        TS_int.(input_names{i}) = TS.(input_names{i});
        
        % remove the samples in the new timeseries object to be replaced by interpolated values
        TS_int.(input_names{i}) = delsample(TS_int.(input_names{i}),'Index',1:length(TS_int.(input_names{i}).Time));
        
        % set the time values to the common_time
        TS_int.(input_names{i}).Time = common_time;
        
        % interpolation using the interp1 function / also interpolates the quality code of the timevariable
        try
            TS_int.(input_names{i}).Data = interp1(TS.(input_names{i}).Time, TS.(input_names{i}).Data, common_time,'linear','extrap');
            
            if ~isempty(TS.(input_names{i}).Quality)
                qualityvals = interp1(TS.(input_names{i}).Time, TS.(input_names{i}).Quality, common_time,'nearest');
                qualityvals(isnan(qualityvals)) = -128; % default quality code for nans is -128
                TS_int.(input_names{i}).Quality = qualityvals;
            end
            
        catch exception
            if ~isempty(common_time)
                if (length(TS.(input_names{i}).Time) < 2) && (length(common_time) == length(TS.(input_names{i}).Time)) && (abs(TS.(input_names{i}).Time - common_time) <= eps)
                    TS_int.(input_names{i}).Data = TS.(input_names{i}).Data;
                else
                    TS_int.(input_names{i}).Data = nan(length(common_time),1);
                end
            end
            %warning(exception.message);
        end

    end
   


end