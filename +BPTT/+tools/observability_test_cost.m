function [ out ] = observability_test_cost(x0,obj,plotflag)
% cost function for testing the observability of soft-sensors
% (c) Aydin Golabgir
% Inputs:
    
    td = obj.Timeframe;
    
    % extract input variables from the bioprocess object
    u = obj.u;

    
    % extract experimenta data from the experiment/bioprocess
    exp = fieldnames(obj.Experiments);
    exp_data = obj.Experiments.(exp{1}).getvars(obj.comp_data,'Quality',obj.Quality);
    
    for i=1:length(obj.comp_data)
        % convert units of experimental data from days to hours
        exp_data.(obj.comp_data{i}).Time = (exp_data.(obj.comp_data{i}).Time - obj.Experiments.(exp{1}).MetaData.StartTime) .* 24;
        
        % cut out the experimental data which is out of the range of interest
        %exp_data.(obj.comp_data{i}) = getsampleusingtime(exp_data.(obj.comp_data{i}),td(1),td(end));
    
        
        
        % adjust frequency of experimental data if specified
        freq = exp_data.(obj.comp_data{i}).Length / exp_data.(obj.comp_data{i}).Time(end);
        if ~isempty(obj.data_freq)
            downsample = round(freq/obj.data_freq(i));
        else
            downsample=1;
        end
        
        
        
        if downsample==0,downsample=1;end
        new_time = exp_data.(obj.comp_data{i}).Time(1:downsample:end);
        exp_data.(obj.comp_data{i}) = resample(exp_data.(obj.comp_data{i}),new_time);
        
        % add random noise to experimental data
        % exp_data.(obj.comp_data{i}).Data = exp_data.(obj.comp_data{i}).Data +  exp_data.(obj.comp_data{i}).Data .* randn .* obj.meas_error(i);
        
    end
    
    obj.exp_data = exp_data;


    % find the index of model outputs to be compared
    model_idx = obj.getOutputIdx;

    % replace initial values
    
    % find the index of the included states
    for i=1:length(obj.x_selected)
        p.states_incl(i) = find(strcmp(fieldnames(obj.Model.y),obj.x_selected{i}));
    end
    
    % replace the value of initial conditions with the ones being optimized
    xint = obj.xint;
    xint(p.states_incl) = x0;
    
    % perform a simulation with model and experimental inputs
    warning off
    tic
    [mT,ysim,xsim]=BPTT.tools.model_sim_conf(u,xint,td,obj.Model,1);
    elapsed = toc;
    warning on
    
    if obj.verbose == 1
        disp(['Simulation time:' num2str(elapsed) ' sec'])
    end

    % add simulation results to the MA object
    if plotflag
        obj.mT = mT;
        obj.xsim=cat(3,obj.xsim,xsim);
        obj.ysim=cat(3,obj.ysim,ysim);
    end

    % calculate a fitness/likelihood for each measurement
    L = zeros(length(obj.comp_data),1);
    for i=1:length(L)

        % making sure experimental data exist
        if exp_data.(obj.comp_data{i}).Length>0
            
            try
            % interpolate the simulated outputs onto the measured time
            ysim_interp = interp1(mT,ysim(model_idx(i),:),exp_data.(obj.comp_data{i}).Time);

            % parameters for the likelihood calculation
            mu = exp_data.(obj.comp_data{i}).Data;
            sigma = obj.meas_error(i) .* ones(length(exp_data.(obj.comp_data{i}).Data),1);
            %sigma(sigma<P.min_meas_error(j))=P.min_meas_error(j);

            % negative log likelihood function
            l = (-1/2)*log(2*pi) - (1/2) .* log(sigma.^2) - 1./(2*sigma.^2) .* (ysim_interp-mu).^2;
            L(i) = -1 * nanmean(l);
            catch
                L(i) = 1e9;
            end
        else
            L(i)=1e9;
        end

    end
    
    
    % returning the output
    out = sum(L);
    
    if plotflag
        obj.Cost = [obj.Cost ; out];
        obj.estimated_p = [obj.estimated_p ; x0];   % store the estimated parameter values
        obj.L = [obj.L ; L'];                        % store the likelihood values in the object
    end
    
    
    % plotting the results
    %if mod(size(obj.Cost,1),50)==0 || plotflag
    if plotflag
    
        % using the plot_variables method which was initially meant to be
        % for parameter estimation - that is why the values generate here will
        % currently overwrite parameter estimation results
        
        figure(1)
        
        obj.plot_variables
        xlim([obj.Timeframe(1) obj.Timeframe(end)])
        drawnow

        %figure(2)
        %obj.plot_estimated
        %drawnow
                
        % find out the index of the selected states
        idx = zeros(1,length(obj.x_selected));
        for j=1:length(obj.x_selected)
            idx(j) = find(strcmp(fieldnames(obj.Model.y),obj.x_selected{j}));
        end
        
        %unit_conversion = obj.Model.state_MW(idx);
        
        x_sel_data = obj.Experiments.(exp{1}).getvars(obj.x_sel_data,'Quality',obj.Quality);
        
        for i=1:length(obj.x_selected)
            % convert units of experimental data from days to hours based on the reftime

                       
            % cut out the experimental data which is out of the range of interest
            %exp_data.(obj.x_sel_data{i}) = getsampleusingtime(exp_data.(obj.x_sel_data{i}),td(1),td(end));
            
            % make a plot of the simulation results
            %figure(1+i) 
            
            figure(2)            
            subplot(2,3,i)
            
            %plot(mT,xsim(:,p.states_incl(i)) .* unit_conversion(i),'r')
            plot(mT,xsim(:,p.states_incl(i)) ,'r')
            
            if i==1,legend('estimated'),end
            %ylabel(strrep([obj.x_selected{i} ' [' obj.Model.state_units{p.states_incl(i)} ']'],'_',' '))
            ylabel(strrep([obj.x_selected{i} ' '],'_',' '))
            xlim([obj.Timeframe(1) obj.Timeframe(end)])
            xlabel('Process Time [h]')
            grid on
            % only plot experimental data if they exist
            % selected states with measurements should be written initially
            % and in the correct order for this to work
            
            if i <= length(obj.x_sel_data)
                
                if x_sel_data.(obj.x_sel_data{i}).Length>0
                    hold on
                    
                    x_sel_data.(obj.x_sel_data{i}).Time = (x_sel_data.(obj.x_sel_data{i}).Time - obj.Experiments.(exp{1}).MetaData.StartTime) .* 24;
                    %x_sel_data.(obj.x_sel_data{i}) = getsampleusingtime(x_sel_data.(obj.x_sel_data{i}),td(1),td(end));
                    
                    
                    
                    if ~strcmp(obj.x_sel_std{i},'')
                        x_sel_std = obj.Experiments.(exp{1}).getvars(obj.x_sel_std(i));
                        %x_sel_std.(obj.x_sel_data{i}).Time = (x_sel_std.(obj.x_sel_std{i}).Time - x_sel_std.(obj.x_sel_std{i}).Time(1)) .* 24;
                        x_sel_std = x_sel_std.(obj.x_sel_std{i}).Data;
                        errorbar(x_sel_data.(obj.x_sel_data{i}).Time,x_sel_data.(obj.x_sel_data{i}).Data,x_sel_std ,'k.')
                    else    
                        plot(x_sel_data.(obj.x_sel_data{i}).Time,x_sel_data.(obj.x_sel_data{i}).Data,'k.');
                    end
                    
                    % calculate the error of the estimation
                    
                    %sim_data = interp1(mT,xsim(:,p.states_incl(i)).*unit_conversion(i),x_sel_data.(obj.x_sel_data{i}).Time);
                    sim_data = interp1(mT,xsim(:,p.states_incl(i)),x_sel_data.(obj.x_sel_data{i}).Time);
                    
                    meas_data = x_sel_data.(obj.x_sel_data{i}).Data;
                    RMSE = sqrt(nanmean((sim_data - meas_data).^2));
                    NRMSE = RMSE/(max(meas_data)-min(meas_data));
                    title(['NRMSE = ' num2str(round(NRMSE*1000)/1000)]);
                    obj.NRMSE(i) = NRMSE;
              
                    if i==1,legend('estimated','measured'),end
                    hold off
                    xlim([obj.Timeframe(1) obj.Timeframe(end)])
                end           
            end
            
            drawnow
            
        end
  
    end
    
end

