% Bioprocess object 
% (c) Aydin Golabgir
% 04.11.2014
%
% See also
% 
% Reference page in Help browser 
% <a href="matlab:doc BPTT.Bioprocess;">doc BPTT.Bioprocess</a>
%   

classdef Bioprocess < handle
    
    properties
        db_id = []
        Comment
        Name
        Variables=struct;           % Struct collection of TimeVariables / Raw and Calculated
        Models=struct;              % Struct collection for models
        MetaData                    % Struct that can be used to store info such as preculture 1 length, etc.
        Calculators=struct;         % Calculators are able to perform calculations using TimeVariable objects and defined functions
        Enabled = 1;                % Enabled/disabled on the ExperimentCollection level
        Process_Start_Time;          % Start Time of bioprocess can be used to cut data
        Process_End_Time;            % Process END Time can be used to cut data
        Calcstatus = true;          % can be switch off (false) to prevent automatic recalculation
    end
    
    properties (SetAccess = private)
        
        ModificationTime       % The time the object was modified
        CreationTime = now;    % The creation time of the object
    end
    
    properties (Dependent = true)

    end
    
    properties (Dependent = true, SetAccess = private)
        MinTime                         % Minimum time point present in the object's variables
        MaxTime                         % Maximum time point present in the object's variables
    end
    
    events
       Modified                         % Event for modification of the bioprocess object
    end
    
    methods
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = Bioprocess()
        % Function for creating a bioprocess object
            obj.CreationTime = now;
            obj.ModificationTime = now;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ConvertTime(obj,varargin)
            % transform all ProcessTimes from days to h with reference to
            % MinTime
            n = input('Do you want convert to hours/days/no conversion, h/d/n:','s'); 
            r = input('Do you want convert to relativate Time by B.MinTime yes/no, Y/N:','s');    
                if n == 'h'
                warning('TimeVariables converted to relative Time in hours, calc functionalities are not supported and no return possible')
                m =input('Do you want to continue, Y/N:','s');
                convfac = 24;
                obj.Calcstatus = false; % to repress the get function from recalculation
                elseif n == 'd'
                convfac = 1/24;
                m = 'Y';
                else
                convfac = 1;    
                m = 'Y';
                end
            var_ids = fieldnames(obj.Variables);
            if r == 'Y'
            reftime = obj.MinTime;
            else
            reftime = 0;
            end 
            if m=='Y' %&& strcmp(obj.Variables.(var_ids{1}).TimeInfo.Unit,'h') == 0;
                for k = 1:numel(var_ids)
                  obj.Variables.(var_ids{k}).Time = (obj.Variables.(var_ids{k}).Time - reftime).*convfac;
                  if n == 'h'
                  obj.Variables.(var_ids{k}).TimeInfo.Units = 'h';
                  elseif n == 'd'
                  obj.Variables.(var_ids{k}).TimeInfo.Units = 'days';   
                  end  
                end
               
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = AddVariable(obj, var_time_series)
        % Function for adding TimeVariable object to a Bioprocess object
            % currently adds only raw variables 
            % output of calculators are add by the AddCalculator method
            fieldname = BPTT.tools.rm_special_characters(var_time_series.Name,'warning',0);
            if strcmp(var_time_series.Type,'raw')
                obj.Variables.(fieldname) = var_time_series; 
            obj.Variables.(fieldname).ModificationTime = now;% refresh the modification time
            %obj.Variables.(var_time_series.Name).ModificationTime = var_time_series.Time(end);
            obj.ModificationTime = now; % refresh the modification time
            end
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = CopyVariable(obj, var_time_series)
        % Function for adding TimeVariable object to a Bioprocess object
        % var_time_series = TimeVariable of Bioprocess            
                fieldname = BPTT.tools.rm_special_characters(var_time_series.Name,'warning',0);
            %if strcmp(var_time_series.Type,'raw')
                obj.Variables.(fieldname) = var_time_series; 
            obj.Variables.(fieldname).ModificationTime = now;% refresh the modification time
            %obj.Variables.(var_time_series.Name).ModificationTime = var_time_series.Time(end);
            obj.ModificationTime = now; % refresh the modification time
            %end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = AddSampletoVariable(obj,varname,t,data)
        % Function for adding a time-value pair to a variable
        % Bioprocess.AddSampletoVariable(varname,t,data)
        % Inputs:   varname = string containing the name of the variable
        %           t: time in datenum format
        %           data: data in a format suitable for timeseries objects
            
            if sum(strcmp(fieldnames(obj.Variables),varname)) ~= 1
               error(['Variable ' varname ' does not exist in the bioprocess object.']); 
            end
            
            % add data samples to the TimeVariable object only if the
            % timestamp is not already in the TimeVariable
            if sum(obj.Variables.(varname).Time == t) == 0
                
                obj.Variables.(varname) = obj.Variables.(varname).addsample('Time',t,'Data',data);
                
%                 TS2 = BPTT.TimeVariable;
%                 TS2.Time = t;
%                 TS2.Data = data;
%                 obj.Variables.(varname) = BPTT.tools.addsample_AGO(obj.Variables.(varname),TS2);

                obj.Variables.(varname).ModificationTime = now; % refresh the modification time of the time variable
                obj.ModificationTime = now; % refresh the modification time of the bioprocess object
                %disp(['>>> Added ' obj.Variables.(varname).Name]);
            end
            
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = AddCalculator(obj, calculator)
        % Function for adding a calculator to a Bioprocess-Object BP
        %   BP.AddCalculator(calculator)
        % Arguments:
        %       calculator a BPTT.Calculator - Object to add.
        % Returns nothing.

            calculator.o = obj;
            
            % add the calculator to Calculators structure
            obj.Calculators.(calculator.Name) = calculator;     
            
            % Add the calculated output to the variable struct of the
            % experiment. Some functions may have more than one outputs
            
            % external function to more efficiently concatenate structures
            % than looping through the second one and recalculating the
            % output of the calculator multiple times.
            obj.Variables = BPTT.tools.catstruct(obj.Variables , obj.Calculators.(calculator.Name).y);
            obj.ModificationTime = now;                % refresh the modification time
             
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = AddModel(obj, model)
        % Function for adding a model struct

            % add the model to the Models structure
            obj.Models.(model.Name) = model;     
            obj.ModificationTime = now;                % refresh the modification time
             
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function model_struct = getModel(obj, model_name)
        % Function for outputting a model struct

            if ~isfield(obj.Models,model_name)
                error(['The model ' model_name ' was not found in Bioprocess ' obj.Name]);
            end
        
            model_struct = obj.Models.(model_name);
        end
           
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function calculateAll(obj)
           % function loops through all variables and recalculates (if necessary depending
           % on the calculator settings) the calculator outputs
           %obj = bp; % overwrites realtime object with bioprocess
           varnames = fieldnames(obj.Variables);
           n = length(varnames);
           for i=1:n
               if strcmp(obj.Variables.(varnames{i}).Type , 'calculated')
                   obj.Calculators.(obj.Variables.(varnames{i}).Calculator).y.(varnames{i});
               end
           end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function TS = getvars(obj,S,varargin)
        % Function for getting variables out of the Variables structure
        % S is a cell containing the names of the variables to be returned
        % TS is structure containing the TimeVariable objects
        
            % check whether Quality selection has been activated as an option
            q_idx = find(strcmp(varargin,'Quality'));
            
            n = length(S); % get the number of elements in S

            % extract the time series objects from the Variables structure
            for i=1:n

                varnames = fieldnames(obj.Variables);
                exists = strcmp(varnames,S{i});

                if sum(exists) > 0
                    
                    % for calculated variables, check the calculator output and update the variable in Variables if necessary
%                     if strcmp(obj.Variables.(S{i}).Type , 'calculated') && obj.Calcstatus
% 
%                         % replace the variable with calculator outputs
%                         %obj.Variables.(S{i}).Calculator
%                         %obj.Calculators.(obj.Variables.(S{i}).Calculator).y
% 
%                         obj.Variables.(S{i}) = obj.Calculators.(obj.Variables.(S{i}).Calculator).y.(S{i}); 
%                         
%                         TS.(S{i}) = obj.Variables.(S{i});
%                         
%                     else
                        TS.(S{i}) = obj.Variables.(S{i});
                        
%                    end
                    
                    % filter the specified quality codes by deleting data
                    % defined quality values in arguments can have more
                    % than one values so selection of more than one quality
                    % code should be possible.
                    
                    if ~isempty(q_idx) 
                        
                        quality_values = varargin{q_idx+1};         % get the quality values to be selected out of the input arguments
                        
                        if ~isempty(quality_values)
                        
                            idx = zeros(TS.(S{i}).Length,1);              % define an index vector that will be 1 for data that stays

                            % compare the quality value of the timevariables with the specified ones
                            for j=1:length(quality_values)
                                idx(TS.(S{i}).Quality == quality_values(j))=1;
                            end

                            % find the position of data to be deleted
                            idx = find(~idx);

                            % delete the data points
                            TS.(S{i}) = delsample(TS.(S{i}),'Index',idx);
                        end
                    end
                    
                else
                    error(['Variable ' S{i} ' not found in the Variables structure of the bioprocess ' obj.Name '.']);
                end
                
            end
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function setquality(obj,varname,criteria,qualitycode)
        % function for setting the quality code of a variable based on its data
        % varname: string with the name of the variable
        % criteria: string containing one or more logical operations examples: 'data>0 & data<10' 
        % if the criteria is satisfied for the data, then the specified quality code will be applied
       
            % check the existence of the variable
            varnames = fieldnames(obj.Variables);
            exists = strcmp(varnames,varname);

            if sum(exists) > 0
            
                
                % applying quality differently for raw vs calculated
                % variables - first calculated
                if strcmp(obj.Variables.(varname).Type,'calculated')
                    
                    calc_name = obj.Variables.(varname).Calculator;
                    % set the external quality code of the calculator object
                    obj.Calculators.(calc_name).QualityExt = {criteria,qualitycode};
                    
                else % applying quality for raw variables
                    % check the data of the variable with the provided criteria
                    data = obj.Variables.(varname).Data;    % variable 'data' is element in criteria-string
                    idx = eval(criteria);   % will evaluate local variable 'data'
                    obj.Variables.(varname).Quality(idx)=qualitycode;
                end
                
                %plot(obj.Variables.(varname).Data,obj.Variables.(varname).Quality,'.') % plot for testing
                
            else
                error(['Variable ' varname ' not found in the Variables structure of the bioprocess ' obj.Name '.']);
            end
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function ImportDatafromBP(obj, remoteBP, vars, varargin)
        % Import data from another bioprocess object sequentially (quasi-online use).
        %   BP.ImportDatafromBP(remoteBP, vars, ...) with 
        %       remoteBP as a BPTT.Bioprocess-Object containing BPTT.TimeVariables with Data and
        %       vars as a cell-array containing the names of the BPTT.TimeVariables to import from.
        %   Can be followed by parameter/value pairs to specify additional properties:
        %   'DownSample' and a (integer) Scalar or Vector (same size as
        %       vars) to specify the new Sample-Index-Steps. (1 == import each
        %       datapoint is assumed if omitted).
        %   'CalcInterval' and a Scalar for the IntervallTime (in [minutes])
        %       to force all Calculators to calculate.
        %   'Quality' and a Marker-Value to import Data with giffen Quality
        %       only.
        %   'PlotFcn' with an function-handle to specify a Plotting-Function
        %       wich is invoced during the import process.
        %
        
            % check whether the linked bioprocess property is empty
            if isempty(remoteBP) || ~isa(remoteBP,'BPTT.Bioprocess')
               error('The remote bioprocess object must be specified.') 
            end
                       
            % check whether variables to be imported exist
            if isempty(vars)
               error('Variables to be imported must be specified.') 
            end
            
            % check whether DownSample has been activated as an option
            idx = find(strcmp(varargin,'DownSample'));
            DownSampleRate = ones(1,length(vars));
            if ~isempty(idx)
                if ~isnumeric(varargin{idx+1})
                    error('Values for Option ''DownSample'' must be numeric')
                elseif numel(varargin{idx+1}) == 1
                    DownSampleRate = DownSampleRate * varargin{idx+1};
                elseif numel(varargin{idx+1}) == length(vars)
                    DownSampleRate = varargin{idx+1};
                else
                    error('Values for Option ''DownSample'' must be a scalar or a vector with same length as argument ''vars''.')
                end
                %varargin{idx} = [];    
                %varargin{idx+1} = [];
            end
            
            % check whether PlotFcn has been activated as an option
            idx = find(strcmp(varargin,'PlotFcn'));
            PlotFcn = [];
            if ~isempty(idx) 
                PlotFcn = varargin{idx+1};
            end
            
            % check whether CalcInterval has been activated as an option
            idx = find(strcmp(varargin,'CalcInterval'));
            CalcInterval = [];
            if ~isempty(idx) 
                CalcInterval = varargin{idx+1};
            end
            
            % check whether Quality has been activated as an option
            idx = find(strcmp(varargin,'Quality'));
            Quality = [];
            if ~isempty(idx) 
                Quality = varargin{idx+1};
            end
            
            % check whether specified variables exist
            for i=1:length(vars)
                if sum(strcmp(fieldnames(remoteBP.Variables) , vars{i})) ~= 1
                    error(['Variable ' vars{i} ' does not exist in the linked bioprocess object.']);
                end
            end
            
            % get the structure containing TimeVariables from the linked
            % bioprocess object. Also downsample the data if specified in
            % the options
            data = remoteBP.getvars(vars,'Quality',Quality); 
            

            
            data_ds = data; % create a new variable for downsampled data
            
            for i=1:length(vars)
                
                idx=1:DownSampleRate(i):data.(vars{i}).Length; % index of elements to stay in each time variable
                data_ds.(vars{i}) = delsample(data_ds.(vars{i}),'Index',1:data_ds.(vars{i}).Length); % remove existing data-value pairs from the structure to be downsampled
                data_ds.(vars{i}).Data = data.(vars{i}).Data(idx);
                data_ds.(vars{i}).Time = data.(vars{i}).Time(idx);
            end
            data = data_ds;
            

            
            total_length=0;
            for i=1:length(vars)
                total_length = total_length + data.(vars{i}).Length;
            end
            
            % concatenate all of the data in TimeVariables into a single
            % matrix containing time in the first column. If data for a
            % variable at a specific time point is not available, it will
            % have a nan entry.
            M = nan(total_length,length(vars)+1); % a matrix to hold the combined data
            
            s = 1; % starting index (row) to write data into M
            for i=1:length(vars)
                r = data.(vars{i}).Length; % get the number of data points to be added
                M(s:s+r-1,1) = data.(vars{i}).Time; % add time points to the first column of the matrix M
                M(s:s+r-1,i+1) = data.(vars{i}).Data; % add the data values to the i+1th column of the matrix M
                s = s + r; % keep track of the row for adding the data of the next variable
            end
            
            % sort the rows of the matrix M according to the time value in
            % the first column
            M = sortrows(M);
            
            % create the necessary time variables in the host bioprocess object
            % if the time variables already exist, they will be replaced.
            for i=1:length(vars)
                obj.Variables.(vars{i}) = data.(vars{i}); % first copy the original time variable
                obj.Variables.(vars{i}) = delsample(obj.Variables.(vars{i}),'Index',1:obj.Variables.(vars{i}).Length); % delete existing data from the TimeVariable
            end
                
            % loop through the matrix M and add data sequentially to the created time variables
            
            tk1 = M(1,1);
            tk2 = 0;
            delta_t = 0;
            
            for i=1:size(M,1)
               for j=2:length(vars)+1 % loop through variables to be added
                  if ~isnan(M(i,j))
                      obj.AddSampletoVariable(vars{j-1},M(i,1),M(i,j)); % add the time-value pair using the inbuilt function
                      %obj.Variables.(vars{j-1}) = obj.Variables.(vars{j-1}).addsample('Time',M(i,1),'Data',M(i,j));
                  end
               end
               
               tk2 = M(i,1);
               delta_t = tk2 - tk1;
               
               if ~isempty(CalcInterval) && delta_t > CalcInterval/24/60;
                   disp (['calculating ... ' num2str(tk2*24) 'h (int ' num2str(delta_t*24) 'h)'])
                   obj.calculateAll;
                   

                   if ~isempty(PlotFcn)
                       disp('Creating a plot ... ')
                       feval(PlotFcn,obj)
                   end
                   
                   tk1 = tk2;
                   
               end
               
               BPTT.tools.prcdone(i,total_length,['Data import from ' remoteBP.Name],1);
               
            end

           
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function mirrorquality(obj,var1,var2)
            % function for mirroring the quality values of one variable to another (var1 to var2) using the time values
            % var1 is the name of the variable that donates quality values
            % var2 is either the name of the receiving variable or it can be 'all'
            
            % check the existence of the variable
            varnames = fieldnames(obj.Variables);
            exists1 = strcmp(varnames,var1);
            if sum(exists1)==0
                error(['Variable ' var1 ' not found in the Variables structure of the bioprocess ' obj.Name '.']);
            end
            
            input_var_struct = obj.getvars({var1});
            input_var = input_var_struct.(var1);
            
            % if 'all' is specified as the second input mirror the quality for all of the variables
            if strcmp(var2,'all')
            	varnames = fieldnames(obj.Variables); % get the list of variable names
                varnames(strcmp(varnames,var1))=[]; % remove the var1 name from list of quality-receiving variables
                
                for i=1:length(varnames) % loop through the variables and mirror the quality
                    
                    % mirroring quality for calculated variables
                    if strcmp(obj.Variables.(varnames{i}).Type,'calculated')
                        
                        calc_name = obj.Variables.(varnames{i}).Calculator;
                        obj.Calculators.(calc_name).QualityExtVec.Time = input_var.Time;
                        obj.Calculators.(calc_name).QualityExtVec.Quality = input_var.Quality;
                        
                    else % mirroring quality for raw variables
                        
                        qualityvals = interp1(input_var.Time , input_var.Quality , obj.Variables.(varnames{i}).Time , 'nearest');
                        qualityvals(isnan(qualityvals)) = -128;
                        obj.Variables.(varnames{i}).Quality = qualityvals;
                        obj.Variables.(varnames{i}).QualityInfo = obj.Variables.(var1).QualityInfo;
                        
                    end
                    
                end
                
            else % the case when mirroring is happening for only one variable
                exists2 = strcmp(varnames,var2);
                if sum(exists2) == 0
                    error(['Variable ' var2 ' not found in the Variables structure of the bioprocess ' obj.Name '.']);
                end
                % interpolate quality using the 'nearest' methed
                qualityvals = interp1(obj.Variables.(var1).Time, obj.Variables.(var1).Quality, obj.Variables.(var2).Time , 'nearest');
                qualityvals(isnan(qualityvals)) = -128;
                obj.Variables.(var2).Quality = qualityvals;
                obj.Variables.(var2).QualityInfo = obj.Variables.(var1).QualityInfo;
               
            end
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function removenonmonotonic(obj)
            % Function for removing all non-monotonically increasing data
            % points from all variables in a bioprocess
            % non monotically increasing data points arise when two data
            % points have the same value for time.
            varnames = fieldnames(obj.Variables);
            for i = 1:length(varnames)
                
                idx = diff(obj.Variables.(varnames{i}).Time)==0;
                idx = find(idx);
                obj.Variables.(varnames{i}) = delsample(obj.Variables.(varnames{i}),'Index',idx);
            end
            
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [RMSE, NRMSE, PRMSE, NRMSE2, NRMSE3 ] = RMSE(obj,s1,s2, varargin)
            % function to calculate various RMSE values indicate first the
            % reference value
            % [RMSE, /max-min, %RMSE, /mean, /max ]
            
            q_idx = find(strcmp(varargin,'Quality'));
            quality_values = [];
            if ~isempty(q_idx) 
                quality_values = varargin{q_idx+1};
                %varargin{q_idx} = [];    
                %varargin{q_idx+1} = [];
            end
            
            I = {s1 s2};
            TS = obj.getvars(I,'Quality',quality_values); % get the variables
            TS = BPTT.tools.interplowest(TS); % interpolate the variables
            D1 = TS.(s1).Data;
            D1(isnan(D1))= [];
            D2 = TS.(s2).Data;
            D2(isnan(D2))= [];
            
            error = D1-D2;
            error = error .^ 2;
            RMSE = sqrt(mean(error));
            NRMSE = RMSE/abs(max(D1)-min(D1));
            PRMSE = RMSE*(100*length(D1)/(sum(D1)));
            NRMSE2 = RMSE/mean(D1);
            NRMSE3 = RMSE/abs(max(D1));
        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function phaseplot(obj,s1,s2, varargin)
            % Function for plotting two variables against each other
            
            % check whether Quality selection has been activated as an option
            q_idx = find(strcmp(varargin,'Quality'));
            quality_values = [];
            if ~isempty(q_idx)
                quality_values = varargin{q_idx+1};
                varargin{q_idx} = [];
                varargin{q_idx+1} = [];
            end
            
            % input for error plotting - can take a third variable
            std_idx = find(strcmp(varargin,'std'));
            s3 = '';
            if ~isempty(std_idx)
                s3 = varargin{std_idx+1};
                varargin{std_idx} = [];
                varargin{std_idx+1} = [];
            end
            
            % input for shifting the x axis by a constant value
            xshift_idx = find(strcmp(varargin,'xshift'));
            xshift = 0;
            if ~isempty(xshift_idx)
                xshift = varargin{xshift_idx+1};
                varargin{xshift_idx} = [];
                varargin{xshift_idx+1} = [];
            end
            
            % if the second input signal is set to all, plot the first
            % against all of the other signals in the bioprocess
            if strcmp(s2,'all')
                vars = fieldnames(obj.Variables);
                vars = vars(~strcmp(vars,s1));
                
                for i=1:length(vars)
                    s2 = vars{i};
                    % make the plot
                    if ~isempty(std_idx) % with error bar
                        I = {s1 s2 s3};
                        TS = obj.getvars(I,'Quality',quality_values); % get the variables
                        TS = BPTT.tools.interplowest(TS); % interpolate the variables
                        errorbar(TS.(s1).Data+xshift,TS.(s2).Data,TS.(s3).Data ,varargin{1})
                    else
                        I = {s1 s2};
                        TS = obj.getvars(I,'Quality',quality_values); % get the variables
                        TS = BPTT.tools.interplowest(TS); % interpolate the variables
                        plot(TS.(s1).Data+xshift,TS.(s2).Data ,varargin{1:end});
                    end
                    
                    hold on
                end
                
                hold off
                xlabel(strrep([TS.(s1).Name ' (' TS.(s1).DataInfo.unit ')'],'_',' '))
                ylabel(strrep([TS.(s2).Name ' (' TS.(s2).DataInfo.unit ')'],'_',' '))
                grid on
                
            else
                % make the plot
                if ~isempty(std_idx) % with error bar
                    I = {s1 s2 s3};
                    TS = obj.getvars(I,'Quality',quality_values); % get the variables
                    TS = BPTT.tools.interplowest(TS); % interpolate the variables
                    errorbar(TS.(s1).Data+xshift,TS.(s2).Data,TS.(s3).Data ,varargin{1})
                else
                    I = {s1 s2};
                    TS = obj.getvars(I,'Quality',quality_values); % get the variables
                    TS = BPTT.tools.interplowest(TS); % interpolate the variables
                    plot(TS.(s1).Data+xshift,TS.(s2).Data ,varargin{1:end});
                end
                
                xlabel(strrep([TS.(s1).Name ' (' TS.(s1).DataInfo.unit ')'],'_',' '))
                ylabel(strrep([TS.(s2).Name ' (' TS.(s2).DataInfo.unit ')'],'_',' '))
                grid on
                
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plot(obj,s1, varargin)
            % Function for plotting one variable against time

            % check whether Quality selection has been activated as an option
            q_idx = find(strcmp(varargin,'Quality'));
            quality_values = [];
            if ~isempty(q_idx) 
                quality_values = varargin{q_idx+1};
                varargin{q_idx} = [];    
                varargin{q_idx+1} = [];
            end
            % use the interplowest method to align the data
            TS = obj.getvars({s1},'Quality',quality_values); % get the variables
            
%                 %if length(TS.(s1).Data)>50 && length(varargin) >= 1
%                     ds =250;
%                     plot(TS.(s1).Time(1:ds:end),TS.(s1).Data(1:ds:end) ,varargin{1:end}(1:ds:end));
%                     %plot(obj.mT, ysim,'k', exp_time(1:ds:end), exp_data(1:ds:end),'k.')
%                     %plot(obj.mT, ysim,'k', exp_time, exp_data,'k.')
%                 else
                    plot(TS.(s1).Time,TS.(s1).Data ,varargin{1:end});
                %end
            % make the plot
            %plot(TS.(s1).Time,TS.(s1).Data ,varargin{1:end},'color','black');
            
            xlabel(strrep(['Time (' TS.(s1).TimeInfo.Unit ')'],'_',' '))
            ylabel(strrep([TS.(s1).Name ' (' TS.(s1).DataInfo.Unit ')'],'_',' '))
            
            grid off
            box off
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        function plot_sdev(obj,s1, varargin)
            % Function for plotting one variable against time with
            % errorbars

            % check whether Quality selection has been activated as an option
            q_idx = find(strcmp(varargin,'Quality'));
            quality_values = [];
            if ~isempty(q_idx) 
                quality_values = varargin{q_idx+1};
                varargin{q_idx} = [];    
                varargin{q_idx+1} = [];
            end
            % use the interplowest method to align the data
            TS = obj.getvars({s1},'Quality',quality_values); % get the variables
            
            if length(TS.(s1).Data) > 5
            lower = (TS.(s1).Data) -(TS.(s1).sdev);
            upper = (TS.(s1).Data) +(TS.(s1).sdev);
                BPTT.plots.ciplot(lower,upper,TS.(s1).Time,[0.8 0.8 0.8])
                hold on
            plot(TS.(s1).Time,TS.(s1).Data ,'color','k')
            hold off
            else
            % make the plot
            errorbar(TS.(s1).Time,TS.(s1).Data,TS.(s1).sdev ,varargin{1:end},'o-','color','black');
            end
            xlabel(strrep(['Time (' TS.(s1).TimeInfo.Unit ')'],'_',' '))
            ylabel(strrep([TS.(s1).Name ' (' TS.(s1).DataInfo.Unit ')'],'_',' '))
            
            grid off
            box off
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          function plot_sdev2(obj,s1, varargin)
            % Function for plotting one variable against time with
            % errorbars

            % check whether Quality selection has been activated as an option
            q_idx = find(strcmp(varargin,'Quality'));
            quality_values = [];
            if ~isempty(q_idx) 
                quality_values = varargin{q_idx+1};
                varargin{q_idx} = [];    
                varargin{q_idx+1} = [];
            end
            % use the interplowest method to align the data
            TS = obj.getvars({s1},'Quality',quality_values); % get the variables
            % make the plot
            errorbar(TS.(s1).Time,TS.(s1).Data,TS.(s1).sdev ,varargin{1:end},'color','black');
            xlabel(strrep(['Time (' TS.(s1).TimeInfo.Unit ')'],'_',' '))
            ylabel(strrep([TS.(s1).Name ' (' TS.(s1).DataInfo.Unit ')'],'_',' '))
            
            grid off
            box off
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plot_missing(obj,varargin)
            
            
            % get list of all variables
            
            if nargin == 1
                sigs = fieldnames(obj.Variables); 
            else
                sigs = varargin;
            end
            
            % create a matrix for visualization of missing data
            m =zeros(1+1,length(sigs)+1);

                

            for j=1:length(sigs) % looping through signals
                signal = sigs{j}; % find the signal to be looked at

                %check the existence of the variable in the experiment
                if any(strcmp(signal,fieldnames(obj.Variables)))
                    n = ~isnan(obj.Variables.(signal).Data); % not Nan values produce 1
                else
                    n = 0;
                end
                
                if length(size(n))==2 && size(n,2)==1 % check for multidimensional arrays
                    m(1,j) = sum(n) ;%./ length(n);
                elseif length(size(n))==2 && size(n,2)>1
                    m(1,j) = sum(n(:,1)); %./ length(n);
                end
                
                %m(i,j) = log(sum(n)./ length(n));

                %m(i,j) = 1;
                if sum(n) == 0
                    m(1,j)=0;
                end
            end


            % plotting missing data map
            pcolor(m')
            colormap(gray)
            colorbar 
            title('Number of data points')
            set(gca,'YTick',1.5:1:length(sigs)+0.5)
            set(gca,'YTickLabel',sigs)
            set(gca,'XTick',1.5:1:1+1.5)
            %set(gca,'XTickLabel',ids)
            BPTT.tools.set_xtick_label(strrep(obj.Name,'_',' '),45)

        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function plot_timecoverage(obj,varargin)
            
            N = 50; % number divisions of time
            mintime = obj.MinTime;
            maxtime = obj.MaxTime;
            deltat = (maxtime - mintime)/N;
            tvec = mintime:deltat:maxtime;
            
                        
            % get list of all variables

            if nargin == 1
                sigs = fieldnames(obj.Variables); 
            else
                sigs = varargin;
            end

            % create a matrix for visualization of time coverage
            m =zeros(N+1,length(sigs)+1);

            for i=1:length(tvec)-1    
                
                for j=1:length(sigs) % looping through signals
                    signal = sigs{j}; % find the signal to be looked at

                    %check the existence of the variable in the experiment
                    if any(strcmp(signal,fieldnames(obj.Variables)))
                        
                       TS2 = getsampleusingtime(obj.Variables.(signal),tvec(i), tvec(i+1));
                        %TS2.Length
                        
                        n = ~isnan(TS2.Data); % not Nan values produce 1

                    else
                        n = 0;
                    end

                    if length(size(n))==2 && size(n,2)==1 % check for multidimensional arrays
                        m(i,j) = log(sum(n)) ;%./ length(n);
                    elseif length(size(n))==2 && size(n,2)>1
                        m(i,j) = log(sum(n(:,1))); %./ length(n);
                    end
 
                    if sum(n) == 0
                        m(i,j)=0;
                    end
                end
                
            end

            % plotting missing data map
            pcolor(m')
            colormap(gray)
            colorbar 
            title('Number of data points')
            set(gca,'YTick',1.5:1:length(sigs)+0.5)
            set(gca,'YTickLabel',sigs)
            set(gca,'XTick',1.5:1:N+1.5)
            set(gca,'XTickLabel',datestr(tvec))
            
            BPTT.tools.set_xtick_label(str2cell(datestr(tvec(1:end-1)))',45)

        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function MinTime = get.MinTime(obj)
        % gets the earliest (min) time point from all TimeVariables

            ids = fieldnames(obj.Variables);
            mintimemat = Inf(length(ids),1);    
            for i = 1:length(ids)
                try
                    mintimemat(i) = min(obj.Variables.(ids{i}).Time);
                catch
                end
            end
            MinTime = min(mintimemat);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function MaxTime = get.MaxTime(obj)
        % Gets the latest (max) time point from all TimeVariables
            ids = fieldnames(obj.Variables);
            maxtimemat = zeros(length(ids),1);
            for i = 1:length(ids)
                try
                    maxtimemat(i) = max(obj.Variables.(ids{i}).Time);
                catch
                end
            end
            MaxTime = max(maxtimemat);
        end
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function obj_new = CopyBioprocess(obj, starttime, endtime,TYPE,TV)
            % copy the bioprocess and returns new bioprocess as "new_bioprocess".
            % new_bioprocess = bioprocess_obj.CopyBioprocess()
            % starttime = referncetime for copy start
            % endtime = referencetime for copy end
            % TYPE = Datatype to copy 'raw' or 'all'
            
            if starttime == 0 
                starttime = obj.MinTime;
            end
            if  endtime == 0
                endtime = obj.MaxTime;
            end
            
            obj_new      = BPTT.Bioprocess;
            obj_new.Name = obj.Name;
           if exist('TV','var') && strcmp(TV,'all')
            variables = fieldnames(obj.Variables);
           elseif exist('TV','var') 
                    variables{1} = TV;
           else
           variables = fieldnames(obj.Variables);
           end 
            % Create new process and copy all Variables
           if strcmp(TYPE,'all')
            for i=1:length(variables)
                obj_new.CopyVariable(obj.Variables.(variables{i}));
                obj_new.Variables.(variables{i}).Type = 'raw';
                % cut variables to specified start endtime
                del_idx_min = find(obj_new.Variables.(variables{i}).Time < starttime);
                obj_new.Variables.(variables{i}) = obj_new.Variables.(variables{i}).delsample('Index',del_idx_min);
                %obj.Variables.ProcessTime.Data = obj.Variables.ProcessTime.Data - obj.Variables.ProcessTime.
                del_idx_max = find(obj_new.Variables.(variables{i}).Time > endtime);
                obj_new.Variables.(variables{i}) = obj_new.Variables.(variables{i}).delsample('Index',del_idx_max);
                % copy calculator objects to new bioprocess
                obj_new.Calculators = obj.Calculators;
            end
           elseif strcmp(TYPE,'raw')
               for i=1:length(variables)
               obj_new.AddVariable(obj.Variables.(variables{i}));  
               end
               variables = fieldnames(obj_new.Variables);
               for i=1:length(variables) 
               del_idx_min = find(obj_new.Variables.(variables{i}).Time < starttime);
                obj_new.Variables.(variables{i}) = obj_new.Variables.(variables{i}).delsample('Index',del_idx_min);
               del_idx_max = find(obj_new.Variables.(variables{i}).Time > endtime);
                obj_new.Variables.(variables{i}) = obj_new.Variables.(variables{i}).delsample('Index',del_idx_max);
                end
           end       
     
         
    end
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
     
    end
end
