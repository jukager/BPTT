% Inputs:   
%           1. obj: Bioprocess object
%
%           2. x: structure containing the following TimeVariable objects: 
%               [ F_AIR_in [l/min] , F_O2_in [l/min] , yO2_out [%] , yCO2_out [%] ]

%           3. P: (parameter struct):
%                   P.o2_in_o2 = 0.98;      % Oxygen content in the oxygen supply tank
%                   P.yO2AIR = 0.2095;      % yO2AIR    [-]
%                   P.yCO2AIR = 0.0004;     % yCO2AIR   [-]
%                   P.yO2wet = 0.2074;      % yO2wet    [-]
%                   P.VnM = 22.414;         % VnM volume of O2 under normal conditions [l/mol] 

% Outputs: y contains:
%
%       OUR [mol/h]
%       CER [mol/h]


function y = calc_offgas( x , obj)
    % Function for calculating OUR and CER in aerobic cultures where offgas
    % signals and aeration rates are measured.
    % Aydin Golabgir 11.03.2014

    P = obj.p; % get the parameter structure

    % must implement default parameters in case parameters do not come from
    % outside some kind of nargin check.
    if 0
        P.o2_in_o2 = 0.98;      % Oxygen content in the oxygen supply tank
        P.yO2AIR = 0.2095;      % yO2AIR    [-]
        P.yCO2AIR = 0.0004;     % yCO2AIR   [-]
        P.yO2wet = 0.2074;      % yO2wet    [-]
        P.VnM = 22.414;         % VnM volume of O2 under normal conditions [l/mol] 
    end
    
    rO2 = BPTT.TimeVariable;
    rCO2 = BPTT.TimeVariable;
    RQ = BPTT.TimeVariable;
    
    % get the time series and interpolate at the lowest frequency
    x_names = fieldnames(x);
    
    % perform calculation only if data is available
    if x.(x_names{1}).Length > 0 && x.(x_names{2}).Length > 0 && x.(x_names{3}).Length > 0 && x.(x_names{4}).Length > 0
    
        TS = BPTT.tools.interplowest(x);


        F_AIR_in = TS.(x_names{1});%.* 60; % convert to l/h
        F_O2_in = TS.(x_names{2});%.* 60;  % convert to l/h
        yO2_out = TS.(x_names{3});
        yCO2_out = TS.(x_names{4});

        % set the quality of the output signals
        %RQ.Quality = F_AIR_in.Quality;
        %rO2.Quality = F_AIR_in.Quality;
        %rCO2.Quality = F_AIR_in.Quality;
        
        Fn_in = (F_AIR_in + F_O2_in);

        O2_in_adjusted = (F_O2_in * P.o2_in_o2 + F_AIR_in * P.yO2AIR) ./ Fn_in ;

        CO2_in_adjusted = ( F_AIR_in * P.yCO2AIR) ./ Fn_in ;

        % calculate gas rates; convert from [%] to [-]
        O2_offgas = yO2_out ./ 100;
        CO2_offgas = yCO2_out ./ 100;

        % exH2O_out = 1 - (ywet / yO2_in) [-], (constant)
        exH2O_out = 1 - (P.yO2wet / P.yO2AIR);

        % Ra_inert(t) = (1-yO2_in - yCO2_in  ) / (1 - yO2_out - yCO2_out - exH2O_out)
        Ra_inert = (1 - O2_in_adjusted - CO2_in_adjusted) ./ (1 - O2_offgas - CO2_offgas - exH2O_out);

        %Ra_inert = smooth(tint_on,Ra_inert,0.01,'rloess');

        Fn_out = Fn_in .* Ra_inert;
        %Fn_out = Fn_in;

        % convert units from [l/min] to [mol/hour]
        Fn_in = Fn_in .* (1/P.VnM);
        Fn_out = Fn_out .* (1/P.VnM);

        % mdotO2in [mol-O2 / hour]
        mdotO2in = Fn_in .* O2_in_adjusted;

        % mdotO2out [mol-O2 / hour]
        mdotO2out = Fn_out .* O2_offgas;

        % rO2 [mol-O2/h]
        rO2 = mdotO2out - mdotO2in;

        % mdotCO2_in [mol-CO2 / hour]
        mdotCO2_in = Fn_in .* CO2_in_adjusted ;

        % mdotCO2_out [mol-CO2 / hour]
        mdotCO2_out = Fn_out .* CO2_offgas;

        % rCO2 = mdotCO2_out - mdotCO2_in [mol-CO2 / hour]
        rCO2 = mdotCO2_out - mdotCO2_in;

        % calculate RQ
        RQ = -1 .* rCO2 ./ rO2 ;
     
         
        RQ.Quality = TS.(x_names{1}).Quality;
        rCO2.Quality = TS.(x_names{1}).Quality;
        rO2.Quality = TS.(x_names{1}).Quality;    
    end
    
    rO2.Name = 'OUR_calc';
    rO2.DataInfo.unit = 'mol/h';
    rO2.Calculator = 'calc_offgas';
   
    
    rCO2.Name = 'CER_calc';
    rCO2.DataInfo.unit = 'mol/h';
    rCO2.Calculator = 'calc_offgas';
    
    
    RQ.Name = 'RQ_calc';
    RQ.DataInfo.unit = '-';
    RQ.Calculator = 'calc_offgas';
    
    y.(RQ.Name) = RQ;
    y.(rO2.Name) = rO2;
    y.(rCO2.Name) = rCO2;
   

end