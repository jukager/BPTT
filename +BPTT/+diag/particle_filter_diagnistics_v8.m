function particle_filter_diagnistics_v8( yk_n , y_act, qi , qi2, P_q , P_q_res, N_eff, N_eff_res)

    figure(2)
    subplot(3,2,1)
    hist(yk_n)
    title(['Particle filter (' num2str(size(yk_n)) ')'])
    
    subplot(3,2,2)
    hist(y_act)
    title(['Measurements (' num2str(size(y_act)) ')'])
    
    subplot(3,2,3)
    hist(qi)
    title('Original qi')
    
    subplot(3,2,4)
    hist(qi2)
    title('Regularized qi' )
    
    
    subplot(3,2,5)
    hist(P_q)
    title(['P (before res); N effective = ' num2str(N_eff) ' '])
    
    subplot(3,2,6)
    hist(P_q_res)
    title(['P (after res); N resampled = ' num2str(N_eff_res) ' '])

end

